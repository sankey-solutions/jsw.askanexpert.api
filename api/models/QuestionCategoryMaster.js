/**
 * QuestionCategoryMaster.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	tableName:"QuestionCategoryMaster",
	attributes: {
		
		categoryId:{
			type:'string',
			required:true
		},
		categoryName:{
			type:'string',
			required:true
		},
		isDeleted:{
			type:'boolean',
			required:true,
			defaultsTo : false
		},
		subCategories:{
			type:'Array'
		},
		createdBy:{
			type:'string',
			required:true
		},
		updatedBy:{
			type:'string'
		}
			
	},

  addCategory:function(reqbody,next){
	// default response object
    var responseObj = {
      "statusCode": -1,
      "message": null,
      "result": null
    };
    reqbody.categoryId="CA"+Date.now();
	reqbody.subcategories=[];

    QuestionCategoryMaster.create(reqbody).exec(function (err,record){
		if (err) {		// handling for error
			responseObj.message = err ;
			sails.log.error("QuestionCategoryMaster-Model>addCateory>QuestionCategoryMaster.create ","Input:"+ reqbody," error:"+err);
			next(responseObj);  					
		} 
		else if (!record){     // handling in case undefined/no object was returned
			responseObj.message = "undefined object was returned" ;
			sails.log.error("QuestionCategoryMaster-Model>addCateory>QuestionCategoryMaster.create ","Input:"+ reqbody," result:"+record);
			next(responseObj); 	
		}
		else {  	// record created successfully	
			responseObj.statusCode = 0;
			responseObj.message = "Category record successfully created !!";
			responseObj.result = record;
			sails.log.info("QuestionCategoryMaster-Model>addCateory>QuestionCategoryMaster.create ","Input:"+ reqbody," result:"+record);
			next(responseObj);
		}			  	
    })
  },

  getCategory:function(reqBody,next){

	// default response object
    var responseObj = {
		"statusCode": -1,
		"message": null,
		"result": null
	};

	var findCriteria={isDeleted:false};
	if(reqBody.categoryId){
		findCriteria.categoryId=reqBody.categoryId;
	}

	var MyQuery=QuestionCategoryMaster.find(findCriteria);

	if(reqBody.skip){
		MyQuery=MyQuery.skip(reqBody.skip);
	}
	if(reqBody.limit){
		MyQuery=MyQuery.limit(reqBody.limit);
	}
	if(reqBody.sort){
		MyQuery=MyQuery.sort(reqBody.sort);		
	}
	
	MyQuery.exec(function f(err,records){
		if(err){
			responseObj.message = err;
			sails.log.error("QuestionCategoryMaster-Model>getCategory>QuestionCategoryMaster.find ","Input:"+ MyQuery," error:"+err);
			next(responseObj);
		}
		else if (!records){      	// handling in case undefined/no object was returned
			  responseObj.message = "undefined object was returned !!" ;
			  sails.log.error("QuestionCategoryMaster-Model>getCategory>QuestionCategoryMaster.find ","Input:"+ MyQuery," result:"+records);
			next(responseObj); 	
		  }
		else if(records.length===0){		// no record found
			responseObj.statusCode = 2;
			responseObj.message = "No record found !!";
			sails.log.info("QuestionCategoryMaster-Model>getCategory>QuestionCategoryMaster.find ","Clients records fetched successfully !!");		
			next(responseObj);
		}else{							// record fetched successfully
			responseObj.statusCode = 0;
			responseObj.message = "Categories fetched successfully !!";
			responseObj.result = records;
			sails.log.info("QuestionCategoryMaster-Model>getCategory>QuestionCategoryMaster.find ","Clients records fetched successfully !!");		  				
			next(responseObj);
		}
	}) // end of find query
  },

  updateCategory:function(reqBody,next){
	  // default response object
		var responseObj = {
			"statusCode": -1,
			"message": null,
			"result": null
		};

		var findCriteria = {categoryId:reqBody.categoryId};
		var updateInfo = reqBody.updateInfo;
		console.log(findCriteria);
		console.log(updateInfo);
		//if flag is 1--> can delete
		//if flag is 0--> cannot delete as subcategory present
		var flag=0;  //
		// In case record is to be deleted
		if(updateInfo.isDeleted===true){
			//fetching the record for checking if it has subcategory
			QuestionCategoryMaster.getCategory(reqBody,function(data){
				console.log("data----",data);
				//if record is fetched succesfully 
				if(data.statusCode===0){
					//if subcategories array is empty
					var record=data.result;
					var subcategories=record[0].subcategories;
					if(subcategories.length==0){
						flag=1;
					}
					//if atleast one of the subcatgory is not deleted
					else {
						var i;
						flag = 1; // assume all are deleted 
						for (i = 0; i < subcategories.length; i++) { 
							if(subcategories[i].isDeleted==false){
								flag=0;
								break;
							}
						}
					}

					console.log("Value of flag",flag);
					if(flag === 1)
						QuestionCategoryMaster.updateCategoryQuery(findCriteria,updateInfo,responseObj,next);
					else{
						console.log("inside else",flag);
						responseObj.message="Cannot delete!! Subcategory exists for this category.";
						responseObj.result=data.result;
						responseObj.statusCode=0;
						next(responseObj);
					}
				}
				//if some error occurs while fetching the record
				else{
					return next(data);
				}
			})
		}
		else{
			QuestionCategoryMaster.updateCategoryQuery(findCriteria,updateInfo,responseObj,next);
		}
},

	addSubCategory: function(reqBody,next){
		// default response object
		var responseObj = {
			"statusCode": -1,
			"message": null,
			"result": null
		};

		var findCriteria = {categoryId:reqBody.categoryId};
		var updateInfo;
		var subcategoryInfo=reqBody.subcategoryInfo;

		var subcategory={};
		subcategory.subcategoryName=subcategoryInfo.subcategoryName;
		subcategory.subcategoryId="SUBCA"+Date.now();
		subcategory.isDeleted=false;

		QuestionCategoryMaster.getCategory(findCriteria,function(data){
			if(data.statusCode===0){
				var record=data.result;
				console.log("data--------",data);
				subCategories=record[0].subcategories;
				subCategories.push(subcategory);
				updateInfo={subcategories:subCategories}
				QuestionCategoryMaster.update(findCriteria,updateInfo).exec(function (err,records){
					console.log("updateClient: updated record is",records);
					if (err) {
						responseObj.message = err ;
						sails.log.error("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update ","Input:"+ subcategories," error:"+err);
						next(responseObj);  					
					} 
					else if (!record){      	// handling in case undefined/no object was returned
						responseObj.message = "undefined object was returned" ;
						sails.log.error("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update ","Input:"+ subcategories," result:"+records);
						next(responseObj); 	
					}
					else if (records.length === 0){      	// handling in case records was not updated
						responseObj.message = "Category not found!!" ;
						sails.log.error("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update ","Input:"+ subcategories," result:"+records);
						next(responseObj); 	
					}
					else {   						// record successfully updated
						responseObj.statusCode = 0;
						responseObj.message = "Subcategory added successfully!!";
						responseObj.result = records[0];
						sails.log.info("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update : Client record updated successfully!!");
						next(responseObj);
					}			  	
				});
				}  
			else{
				next(data);
			} 
		})
	},


	updateCategoryQuery(findCriteria,updateInfo,responseObj,next){

		QuestionCategoryMaster.update(findCriteria,updateInfo).exec(function (err,records){
			console.log("in update method--------------");
			console.log("updateClient: updated record is",records);
			if (err) {
				console.log("error------------")
				responseObj.message = err ;
				sails.log.error("QuestionCategoryMaster-Model>updateClient>QuestionCategoryMaster.update ","Input:"+ updateInfo," error:"+err);
				next(responseObj);  					
			} 
			else if (!records){      	// handling in case undefined/no object was returned
				responseObj.message = "undefined object was returned" ;
				sails.log.error("QuestionCategoryMaster-Model>updateClient>QuestionCategoryMaster.update ","Input:"+ updateInfo," result:"+records);
				next(responseObj); 	
			}
			else if (records.length === 0){      	// handling in case records was not updated
				console.log("records not found");
				responseObj.message = "Record not found!!" ;
				sails.log.error("QuestionCategoryMaster-Model>updateClient>QuestionCategoryMaster.update ","Input:"+ updateInfo," result:"+records);
				next(responseObj); 	
			}
			else {   		
				console.log("updated");				// record successfully updated
				responseObj.statusCode = 0;
				responseObj.message = "Category updated successfully!!";
				responseObj.result = records;
				sails.log.info("QuestionCategoryMaster-Model>updateClient>QuestionCategoryMaster.update : Client record updated successfully!!");
				next(responseObj);
			}			  	
		})
	},
	

	updateSubCategory:function(reqBody,next){

		var responseObj = {
			"statusCode": -1,
			"message": null,
			"result": null
		};

		var findCriteria = {categoryId:reqBody.categoryId};
		var updateInfo;
		var subcategoryInfo=reqBody.subcategoryInfo;
		QuestionCategoryMaster.getCategory(findCriteria,function(data){
			if(data.statusCode===0){
				var record=data.result;
				console.log("data--------",data);
				subCategories=record[0].subcategories;
				var i;
				var isSubcategoryFound = false;
				for (i = 0; i < subCategories.length; i++) { 
					if(subCategories[i].subcategoryId===subcategoryInfo.subcategoryId){
						if(subcategoryInfo.subcategoryName)
							subCategories[i].subcategoryName=subcategoryInfo.subcategoryName;
						if(subcategoryInfo.isDeleted)
							subCategories[i].isDeleted=subcategoryInfo.isDeleted;
						isSubcategoryFound = true;
						break;
					}
				}

				if(!isSubcategoryFound){
					responseObj.message = "Given subcategory was not found !!";
					return next(responseObj);
				}

				updateInfo={subcategories:subCategories}
				QuestionCategoryMaster.update(findCriteria,updateInfo).exec(function (err,records){
					console.log("updateSubCategory: updated record is",records);
					if (err) {
						responseObj.message = err ;
						sails.log.error("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update ","Input:"+ subcategories," error:"+err);
						next(responseObj);  					
					} 
					else if (!record){      	// handling in case undefined/no object was returned
						responseObj.message = "undefined object was returned" ;
						sails.log.error("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update ","Input:"+ subcategories," result:"+records);
						next(responseObj); 	
					}
					else if (records.length === 0){      	// handling in case records was not updated
						responseObj.message = "Category not found!!" ;
						sails.log.error("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update ","Input:"+ subcategories," result:"+records);
						next(responseObj); 	
					}
					else {   						// record successfully updated
						responseObj.statusCode = 0;
						responseObj.message = "Subcategory updated successfully!!";
						responseObj.result = records[0];
						sails.log.info("QuestionCategoryMaster-Model>updateSubCategory>QuestionCategoryMaster.update : Client record updated successfully!!");
						next(responseObj);
					}			  	
				}); 
			}
			else{
				//if some error occured while fetching data
				next(data);
			}

		})
    },

    getsubCategory: function(reqBody, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        console.log(reqBody);
        var findCriteria = {};
        findCriteria = { "subcategories.isDeleted": false,"subcategories.subcategoryId":reqBody.subcategoryId };
       // if (reqBody.subcategoryId) {
         //   findCriteria.subcategoryId = reqBody.subcategoryId;
        //}
        sails.log.info("findCriteria is" );
        sails.log.info(findCriteria);
        var MyQuery = QuestionCategoryMaster.find(findCriteria);

        if (reqBody.skip) {
            MyQuery = MyQuery.skip(reqBody.skip);
        }
        if (reqBody.limit) {
            MyQuery = MyQuery.limit(reqBody.limit);
        }
        if (reqBody.sort) {
            MyQuery = MyQuery.sort(reqBody.sort);
        }

        MyQuery.exec(function f(err, records) {
                if (err) {
                    responseObj.message = err;
                    sails.log.error("QuestionCategoryMaster-Model>getsubCategory>QuestionCategoryMaster.find ", "Input:" + MyQuery, " error:" + err);
                    next(responseObj);
                } else if (!records) { // handling in case undefined/no object was returned
                    responseObj.message = "undefined object was returned !!";
                    sails.log.error("QuestionCategoryMaster-Model>getsubCategory>QuestionCategoryMaster.find ", "Input:" + MyQuery, " result:" + records);
                    next(responseObj);
                } else if (records.length === 0) { // no record found
                    responseObj.statusCode = 2;
                    responseObj.message = "No record found !!";
                    sails.log.info("QuestionCategoryMaster-Model>getsubCategory>QuestionCategoryMaster.find ", "Clients records fetched successfully !!");
                    next(responseObj);
                } else { // record fetched successfully
                    console.log(records);
                    responseObj.statusCode = 0;
                    responseObj.message = "Categories fetched successfully !!";
                    responseObj.result = records;
                    sails.log.info("QuestionCategoryMaster-Model>getsubCategory>QuestionCategoryMaster.find ", "Clients records fetched successfully !!");
                    next(responseObj);
                }
            }) // end of find query
    },
}