/**
 * ForumQuestions.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var request = require('request');
var async = require('async');
var arrayDifference = require('array-difference');
var _ = require('lodash');

module.exports = {
    tableName: 'ForumQuestions',
    attributes: {

        question: {
            type: 'string'
        },
        state: {
            type: 'string'
        },
        city: {
            type: 'string'
        },
        categoryId: {
            type: 'string'
        },
        subCategoryId: {
            type: 'string'
        },
        questionId: {
            type: 'string',
            required: true
        },
        isDeleted: {
            type: 'boolean',
            defaultsTo: false
        },
        answeredBy: {
            type: 'string',
        },
        isAnswered: {
            type: 'boolean',
            defaultsTo: false
        }
    },

    addForumQuestion: function(reqBody, token, programInfo, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };
        console.log("token  ", token);

        var programInfo = programInfo;
        var recordInfo = reqBody;
        var questionId = "QUES" + Date.now();
        // var engineerIds = [];
        var programUserInfo = [];
        var state = reqBody.state;
        var city = reqBody.city;
        var programId = reqBody.programId;
        var clientId = reqBody.clientId;
        var engineerId = reqBody.engineerId;
        recordInfo.questionId = questionId;
        var businessEntityUserDetails = [];
        // var engineerEmails = [];
        var engineerUserIds = [];
        // var sendEmailArray = [];

        console.log("recordInfo", recordInfo);


        var requestObject2 = {
            clientId: clientId,
            programId: programId,
            state: state,
            city: city,
            serviceType: "programSetup",
            engineerProgramRole: "BR1519707374235",
            engineerId: engineerId,
            frontendUserInfo: {},
            profileId: "UP1531873691460"
        }

        // Add question to expert engg for particular state is present
        ForumQuestions.addQuestionToExpert(requestObject2, token, programInfo, recordInfo, function(response) {
            // console.log("response-----------------",response);
            if (response.statusCode === 0) {
                next(response);
            }

            //If expert not found in a particular state
            else if (response.statusCode === 2) {

                var requestObject3 = {
                    clientId: clientId,
                    programId: programId,
                    serviceType: "programSetup",
                    engineerProgramRole: "BR1519707374235",
                    engineerId: engineerId,
                    frontendUserInfo: {},
                    profileId: "UP1531873691460"
                }

                //Add question to all expert if there is no expert engg in that particular state 
                ForumQuestions.addQuestionToExpert(requestObject3, token, programInfo, recordInfo, function(response) {
                    // console.log("response-----------------",response);
                    if (response.statusCode === 0) {
                        response.message = "Question successfully added to all expert engineer";
                        next(response);
                    }

                });

            } else {
                // console.log("response-----------------",response);
                next(response);
            }

        });

    },

    //Functionality to add question to expert of particulur state or to all expert if there is no expert engg in that state 
    addQuestionToExpert: function(requestObject, token, programInfo, recordInfo, next) {
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };
        var engineerIds = [];
        var sendEmailArray = [];
        var engineerEmails = [];

        ForumQuestions.getProgramUserId(requestObject, token, function(response) {
            //console.log("response...................", response);
            if (response.statusCode === 0) {
                programUserInfo = response.result;
                // responseObj.result = programUserInfo;
                console.log("engineers in maharashtra", programUserInfo);

                for (var a = 0; a < programUserInfo.length; a++) {
                    var tempObj = {};
                    tempObj.programUserId = programUserInfo[a].programUserId;
                    engineerIds.push(programUserInfo[a].programUserId);
                    if (programUserInfo[a].businessEntityUserDetails) {

                        for (var b = 0; b < programUserInfo[a].businessEntityUserDetails.length; b++) {
                            if (programUserInfo[a].businessEntityUserDetails[b].userType === "owner") {
                                tempObj.email = programUserInfo[a].businessEntityUserDetails[b].email;
                                // engineerEmails.push(programUserInfo[a].businessEntityUserDetails[b].email);
                                tempObj.userId = programUserInfo[a].businessEntityUserDetails[b].userId;
                                // engineerUserIds.push(programUserInfo[a].businessEntityUserDetails[b].userId);
                                sendEmailArray.push(tempObj);
                            }
                        }
                    }
                }
                // responseObj.result = engineerIds;
                recordInfo.engineers = engineerIds;
                // recordInfo.engineerEmails = engineerEmails;
                // creating the record
                ForumQuestions.create(recordInfo).exec(function(err, record) {
                    console.log("record", record, "error", err);
                    if (err) { // handling for error
                        responseObj.message = err;
                        sails.log.error("ForumQuestions-Model>addForumQuestion>ForumQuestions.create", "Input:" + recordInfo, " error:" + err);
                        next(responseObj);
                        // callback("error in function 2", "function 2 failed");
                    } else if (!record) { // handling in case undefined/no object was returned
                        responseObj.message = "undefined object was returned";
                        sails.log.error("ForumQuestions-Model>addForumQuestion>ForumQuestions.create", "Input:" + recordInfo, " result:" + record);
                        next(responseObj);
                        // callback("error in function 2", "function 2 failed");
                    } else { // record created successfully  
                        responseObj.statusCode = 0;
                        responseObj.message = "Question added successfully.";
                        responseObj.result = record;
                        responseObj.emails = engineerEmails;
                        next(responseObj);
                        for (var z = 0; z < sendEmailArray.length; z++) {
                            var emailInfo = {};
                            emailInfo.programName = programInfo.programName;
                            emailInfo.programId = programInfo.programId;
                            emailInfo.programUserId = sendEmailArray[z].programUserId;
                            emailInfo.clientId = clientId;
                            emailInfo.emailIds = [sendEmailArray[z].email];
                            emailInfo.userIds = [sendEmailArray[z].userId];
                            emailInfo.subject = "A query has been added";
                            emailInfo.html = "<p>" + "A question that you can answer on the forum. Please visit JSW portal to answer the question." + "<br>" + "Question:" +
                                record.question + "<p>";
                            emailInfo.isProgramEmail = true;
                            emailInfo.programEmailId = programInfo.programEmailId
                                // if (response.emailInfo.claimApproval.isActive) {
                            CommunicationService.sendEmail(emailInfo, token);
                            // }
                        }
                        // LogService.addDataLog(dataLogObject, token);
                        sails.log.info("ForumQuestions-Model>addForumQuestion>ForumQuestions.create", responseObj.message);
                    }
                }); // end of SecondarySales.create
            } else {
                // callback("response----------", response);
                next(response);
            }
        })
    },

    updateForumQuestion: function(reqBody, token, programInfo, next) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        var programId = reqBody.programId;
        var clientId = reqBody.clientId;
        var updateInfo = reqBody.updateInfo;
        var questionId = reqBody.questionId;
        var isReallocation = false;
        var newEngineers;
        var oldRecord = null; //to store old record
        var newRecord;

        // find criteria for update operation
        var findCriteria = {
            questionId: questionId
        };
        console.log("updateInfo.engineers", updateInfo.engineers);
        if (updateInfo.engineers) {
            isReallocation = true;
            newEngineers = updateInfo.engineers;
        }
        console.log("isReallocation", isReallocation);
        console.log("newEngineers", newEngineers);
        async.series([
                // function1: to store old record
                function(callback) {
                    console.log("inside function 1");
                    if (isReallocation) {
                        var requestObject = {
                            questionId: reqBody.questionId
                        }
                        console.log("getForumQuestions requestObject", requestObject);
                        // get existing questions record
                        ForumQuestions.getForumQuestions(requestObject, function(response) {
                            console.log("getForumQuestions response", response);
                            if (response.statusCode === 0) {
                                oldRecord = response.result[0];
                                callback(null); //async series callback
                            } else {
                                callback("record not found"); //async series callback
                            }
                        });
                    } else {
                        callback(null); //async series callback
                    }

                },

                // function2: to update questions info
                function(callback) {
                    console.log("inside function 1");
                    // updating the record
                    console.log("updateInfo", updateInfo);
                    ForumQuestions.update(findCriteria, updateInfo).exec(function(err, records) {
                        console.log("update", err, records);
                        if (err) {
                            responseObj.message = err;
                            sails.log.error("ForumQuestions-Model>updateForumQuestions>ForumQuestions.update ", "Input:" + updateInfo, " error:" + err);
                            next(responseObj);
                            callback("error in update", "function 1 failure");
                        } else if (!records) { // handling in case undefined/no object was returned
                            responseObj.message = "undefined object was returned";
                            sails.log.error("ForumQuestions-Model>updateForumQuestions>ForumQuestions.update ", "Input:" + updateInfo, " result:" + records);
                            next(responseObj);
                            callback("error in update", "function 1 failure");
                        } else if (records.length === 0) { // handling in case records was not updated
                            responseObj.message = "Record not found!!";
                            sails.log.error("ForumQuestions-Model>updateForumQuestionsForumQuestions.update ", "Input:" + updateInfo, " result:" + records);
                            next(responseObj);
                            callback("error in update", "function 1 failure");
                        } else { // record successfully updated
                            responseObj.message = "Record updated!";
                            responseObj.result = records;
                            newRecord = records[0];
                            responseObj.statusCode = 0;
                            var getReqObject = {
                                "questionId": newRecord.questionId
                            }
                            console.log("getReqObject", getReqObject);
                            // to get program user info for engineers
                            ForumQuestions.getForumQuestions(getReqObject, function(response) {
                                console.log("get forum questions response", response);
                                next(response);
                            })
                            callback(null);
                        }
                        // next(responseObj);


                    });
                },
                // function3:to check if there is reallocation of engineers
                // if there is reallocation, find if there are new engineers allocated.
                // if yes,send them email notification.else, do nothing.
                function(callback) {
                    console.log("inside function 3");
                    if (isReallocation) {
                        console.log("oldRecord", oldRecord);
                        if (oldRecord !== null) {
                            var oldEngineers = oldRecord.engineers;
                            // var engineersToMail = arrayDifference(newEngineers, oldEngineers);
                            var engineersToMail = _.difference(newEngineers, oldEngineers);
                            console.log("newEngineers", newEngineers);
                            console.log("oldEngineers", oldEngineers);
                            console.log("engineersToMail", engineersToMail);
                            async.each(engineersToMail, function(engineerId, anyncEachCb) {
                                console.log("engineerId", engineerId);
                                // reqobjct to get program user details
                                var reqObject = {
                                    clientId: clientId,
                                    programId: programId,
                                    serviceType: "programSetup",
                                    engineerProgramRole: "BR1519707374235",
                                    engineerId: engineerId,
                                    frontendUserInfo: {},
                                    profileId: "UP1531873691460"
                                }
                                var sendEmailArray = []; //engineers mails to send email
                                console.log("getProgramUserId reqObject", reqObject);
                                ForumQuestions.getProgramUserId(reqObject, token, function(response) {
                                    console.log("getProgramUserId response", response);
                                    if (response.statusCode === 0) {
                                        programUserInfo = response.result;
                                        console.log("programUserInfo.businessEntityUserDetails", programUserInfo.businessEntityUserDetails);
                                        // to form mail arrays for users
                                        for (var a = 0; a < programUserInfo.length; a++) {
                                            var tempObj = {};
                                            console.log("programUserInfo[a]", programUserInfo[a]);
                                            tempObj.programUserId = programUserInfo[a].programUserId;
                                            if (programUserInfo.businessEntityUserDetails) {
                                                for (var b = 0; b < programUserInfo.businessEntityUserDetails.length; b++) {
                                                    if (programUserInfo.businessEntityUserDetails[b].userType === "owner") {
                                                        tempObj.email = programUserInfo.businessEntityUserDetails[b].email;
                                                        tempObj.userId = programUserInfo.businessEntityUserDetails[b].userId;
                                                        console.log("tempObj", tempObj);
                                                        sendEmailArray.push(tempObj);
                                                    }
                                                }
                                            }

                                        }
                                        console.log("final sendEmailArray", sendEmailArray);
                                        console.log("newRecord", newRecord);
                                        // to form mail info and send mail to engineers
                                        for (var z = 0; z < sendEmailArray.length; z++) {
                                            var emailInfo = {};
                                            emailInfo.programName = programInfo.programName;
                                            emailInfo.programId = programInfo.programId;
                                            emailInfo.programUserId = sendEmailArray[z].programUserId;
                                            emailInfo.clientId = clientId;
                                            emailInfo.emailIds = [sendEmailArray[z].email];
                                            emailInfo.userIds = [sendEmailArray[z].userId];
                                            emailInfo.subject = "A query has been added";
                                            emailInfo.html = "<p>" + "A question that you can answer on the forum. Please visit JSW portal to answer the question." + "<br>" + "Question:" +
                                                newRecord.question + "<p>";
                                            emailInfo.isProgramEmail = true;
                                            emailInfo.programEmailId = programInfo.programEmailId
                                            console.log("emailInfo", emailInfo);
                                            CommunicationService.sendEmail(emailInfo, token);
                                        }
                                        anyncEachCb(); //asnyc each callback

                                    }
                                })
                            }, function(err) {
                                callback(null); //async series callback
                            })
                        }
                    } else {
                        callback(null); //async series callback
                    }
                },
            ],
            function(err) {
                if (err) {
                    console.log("err", err);
                }
                console.log("async series final function");
            }
        )


    },

    /*
     **  methodName: getLeads
     **  Description: to get Leads.
     */
    getForumQuestions: function(requ, next) {
        // console.log("request", requ);
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var skip = requ.skip;
        var limit = requ.limit;
        var sort = requ.sort;
        var stateArray = requ.stateArray;
        var cityArray = requ.cityArray;
        var questionId = requ.questionId;
        var queryString = [{
            $match: {
                isDeleted: false
            }
        }];

        var engineerId = requ.engineerId;

        var countQueryString = { isDeleted: false };

        // specific question
        if (questionId) {
            queryString[0].$match.questionId = questionId;
            countQueryString.questionId = questionId;
        }

        if (stateArray || cityArray) {
            queryString[0].$match.$or = [];
            countQueryString.or = [];
            if (stateArray) {
                queryString[0].$match.$or.push({ state: { "$in": stateArray } })
                countQueryString.or.push({ state: stateArray });
            }
            if (cityArray) {
                queryString[0].$match.$or.push({ city: { "$in": cityArray } });
                countQueryString.or.push({ city: cityArray });
            }
        }

        if (engineerId) {
            queryString[0].$match.engineers = engineerId;
        }

        if (skip !== undefined) {
            queryString.push({ $skip: skip });
        }

        if (limit !== undefined) {
            queryString.push({ $limit: limit });
        }

        // Lookup to get category and subcategory info
        queryString.push({
            $lookup: {
                from: "QuestionCategoryMaster",
                localField: "categoryId",
                foreignField: "categoryId",
                as: "categoryInfo"
            }
        });

        // Lookup to get category and subcategory info
        queryString.push({
            $lookup: {
                from: "ProgramUsers",
                localField: "engineers",
                foreignField: "programUserId",
                as: "userInfo"
            }
        });

        ForumQuestions.native(function(err, collection) {
            if (err) {
                responseObj.message = err;
                next(responseObj);
            } else {
                collection.aggregate(queryString, function(err, records) {
                    responseObj.queryString = queryString;
                    console.log("records,err", records, err);
                    if (err) {
                        responseObj.message = err;
                        next(responseObj);
                    } else if (!records) {
                        responseObj.message = "undefined object was returned";
                        next(responseObj);
                    } else if (records.length > 0) {
                        responseObj.statusCode = 0;
                        responseObj.message = "ForumQuestions fetched successfully";
                        responseObj.result = records;


                        CommonOperations.findCount("ForumQuestions", countQueryString, function(response) {
                            // console.log("Response******************************8", response);
                            if (response.statusCode === 0) {
                                responseObj.statusCode = 0;
                                // responseObj.message = "Count Fetched"
                                // responseObj.result = responseObj.count;
                                responseObj.countQueryString = countQueryString;
                                console.log("QueryStraing", responseObj.countQueryString);
                                responseObj.count = response.result; // adding count to responseobj
                            } else {
                                responseObj.message = "No Records found";
                            }
                            next(responseObj);
                        }); // end of count code
                        // next(responseObj);
                    } else {
                        responseObj.statusCode = 2;
                        responseObj.message = "Records not found";
                        next(responseObj);
                    }
                })
            }
        })
    },

    getProgramUsers: function(requestObject, userLogObject, token, next) {

        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };
        var apiURL = ServConfigService.getApplicationConfig().base_url + ":" +
            ServConfigService.getApplicationAPIs().getAllProgramUsers.port +
            ServConfigService.getApplicationAPIs().getAllProgramUsers.url;

        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().getAllProgramUsers.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: requestObject
        };
        // console.log("requestOptions", requestOptions);
        request(requestOptions, function(error, response, body) {
            //console.log("body ..............................", body);
            if (error || body === undefined) {
                responseObj.statusCode = 2;
                responseObj.message = "Error occurred while validating program user !!";
            } else {
                if (body.statusCode === 0) {
                    responseObj.statusCode = 0;
                    responseObj.message = "Program user fetched successfully!!";
                    responseObj.result = body.result;
                } else if (body.statusCode === 2) {
                    responseObj.statusCode = 2;
                    responseObj.message = "Failed to fetch program";
                } else {
                    responseObj = body;
                }
            }
            next(responseObj);
        })
    },

    //  Method: validateUsersProgramMapping
    //  Description: to check whether users are mapped to program
    getProgramUserId: function(requ, token, next) {
        // default response object
        console.log("%%%%%%%%%%%%%%reqyuest", requ);
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };

        if (requ.engineerId) {

            ProgramUsers.showexpertProfileDetails(requ, function(response) {
                if (response.statusCode == 2) {

                    responseObj.statusCode = 0;

                    var userType = [];
                    var businessEntityUserDetails = [];
                    var userObj = {
                        userType: "owner",
                        email: response.result[0].emailId,
                        userId: response.result[0].userId
                    }

                    businessEntityUserDetails.push(userObj);
                    response.result.businessEntityUserDetails = businessEntityUserDetails;
                    responseObj.result = response.result;
                    next(responseObj);
                }
            });
        } else {
            //api to get all program users
            var base_url1 = ServConfigService.getApplicationConfig().base_url;
            // if (base_url1 == "http://localhost") {
            //     base_url1 = 'http://testwinserver.annectos.net';
            // }

            console.log("[Suman] base_url1 ", base_url1);
            var apiURL =
                //ServConfigService.getApplicationConfig().base_url + ":" +
                base_url1 + ":" +
                ServConfigService.getApplicationAPIs().getProgramUserId.port +
                ServConfigService.getApplicationAPIs().getProgramUserId.url;
            var programId = requ.programId;
            var clientId = requ.clientId;
            var usersIdArray = requ.usersIdArray;
            var uniqueCustomerCode = requ.uniqueCustomerCode;
            var usersIdArray = requ.usersIdArray;
            var mobileNumber = requ.mobileNumber;
            var state = requ.state;
            var city = requ.city;
            var engineerProgramRole = requ.engineerProgramRole;

            var profileId = requ.profileId;


            //request object
            var requestObject = {
                programId: programId,
                // programUsersIdArray: usersIdArray,
                frontendUserInfo: {},
                serviceType: "programSetup",
                clientId: clientId
            };
            if (uniqueCustomerCode) {
                requestObject.uniqueCustomerCode = uniqueCustomerCode;
            }
            if (usersIdArray) {
                requestObject.programUsersIdArray = usersIdArray;
            }
            if (mobileNumber) {
                requestObject.searchByMobile = mobileNumber;
            }
            if (engineerProgramRole) {
                requestObject.programRole = engineerProgramRole;
            }
            if (city) {
                requestObject.city = city;
            }
            if (state) {
                requestObject.state = state;
            }

            if (profileId) {
                requestObject.profileId = profileId;
            }

            //request options to fetch program user
            var requestOptions = {
                url: apiURL,
                method: ServConfigService.getApplicationAPIs().getProgramUserId.method,
                headers: {
                    authorization: "Bearer " + token
                },
                json: requestObject
            };

            console.log("requestOptions", requestOptions);
            // making the service call to get program users
            request(requestOptions, function(error, response, body) {
                console.log("Error is", error, "response body is", body);
                // //console.log("parents", body.result[1].parentUsersInfo);

                if (error) {
                    // error occured
                    responseObj.message =
                        "Error occured in fetching program users:" + error;
                } else if (body) {
                    // pass the response body as it is
                    responseObj = body;
                } else {
                    responseObj.message = "No response due to unknown error";
                }
                next(responseObj);
            });
            // end of request
        }
    },

    displaylastForumQues: function(skip, limit, next) {
        var responseObject = {
            "statusCode": -1,
            "message": null,
            "result": null
        } /*variable defined to send response*/

        var queryString = [{
            $sort: {
                "updatedAt": -1
            }
        }];

        if (skip !== undefined) {
            queryString.push({ $skip: skip });
        };

        if (limit !== undefined) {
            queryString.push({ $limit: limit });
        };

        queryString.push({
            $lookup: {
                "from": "ForumAnswers",
                "localField": "questionId",
                "foreignField": "questionId",
                "as": "user1"
            }
        });

        queryString.push({
            $lookup: {
                from: "ProgramUsers",
                localField: "user1.userId",
                foreignField: "programUserId",
                as: "userInfo"
            }
        });
        queryString.push({
            $unwind: {
                path: "$userInfo",
                preserveNullAndEmptyArrays: true // optional
            }
        });
        queryString.push({
            $project: {
                "_id": 0,
                "questionId": "$questionId",
                "question": "$question",
                "answerId": "$user1.answerId",
                "answer": "$user1.answer",
                "question_askedby": "$frontendUserInfo.name",
                "question_emailIdby": "$frontendUserInfo.email",
                "question_mobileNumberby": "$frontendUserInfo.mobileNumber",
                "question_zipcodeby": "$frontendUserInfo.zipcode",
                "isAnswered": "$isAnswered",
                "updatedAt": "$updatedAt",
                "programUserId": "$userInfo.programUserId",
                "userId": "$userInfo.userDetails.userId",
                "answeredBy": "$userInfo.userDetails.userName",
                "MobileNumber": "$userInfo.userDetails.mobileNumber",
                "emailId": "$userInfo.userDetails.email",
                "state": "$userInfo.userDetails.state",
                "city": "$userInfo.userDetails.city"
            }
        });
        /* 
                queryString.push({
                    $unwind: {
                        path: "$answerId",
                        preserveNullAndEmptyArrays: true // optional
                    }
                });

                queryString.push({
                    $unwind: {
                        path: "$answer",
                        preserveNullAndEmptyArrays: true // optional
                    }
                });
        */

        console.log("String 11111 ", queryString);
        /*We need to fetch last 5 questions and make a lookup and get the 
         * last answer for this question and merge the question and answer 
         * in array and send the result */
        ForumQuestions.native(function(err, collection) {
            if (err) {
                responseObject.statusCode = -1;
                responseObject.message = "Server Error";
                next(responseObject);
            } else {
                console.log("In aggregate")
                collection.aggregate(queryString, function(err, lastquestionIdResult) {
                    if (err) {
                        console.log("Is it coming here", err);
                        responseObject.statusCode = -1;
                        responseObject.message = "Server Error";
                        next(responseObject);
                    } else {
                        console.log("Last 5 QuestionId received", lastquestionIdResult);
                        responseObject.statusCode = 0;
                        responseObject.message = "Last 5 Question and Answer fetched";
                        responseObject.result = lastquestionIdResult;
                        next(responseObject);
                    } //endof function callback
                });
            }
        });
    },

    searchForumQues: function(searchtext, skip, limit, next) {
        var responseObject = {};
        console.log(searchtext);

        var queryString = [{
            $match: {
                $text: { $search: searchtext }
            }
        }];

        queryString.push({
            $sort: {
                "updatedAt": -1
            }
        });

        if (skip !== undefined) {
            queryString.push({ $skip: skip });
        }

        if (limit !== undefined) {
            queryString.push({ $limit: limit });
        }

        queryString.push({
            $project: {
                "question": "$question",
                "questionId": "$questionId",
                "categoryId": "$categoryId",
                "subCategoryId": "$subCategoryId"
            }
        });

        queryString.push({
            $lookup: {
                "from": "ForumAnswers",
                "localField": "questionId",
                "foreignField": "questionId",
                "as": "user1"
            }
        });

        queryString.push({
            $project: {
                "question": "$question",
                "questionId": "$questionId",
                "answerId": { "$slice": ["$user1.answerId", -1] },
                "answer": { "$slice": ["$user1.answer", -1] }
            }
        });

        console.log("Details of query  ", queryString);
        ForumQuestions.native(function(err, collection) {
            if (err) {
                responseObject.statusCode = -1;
                responseObject.message = "Server Error";
                next(responseObject)
            } else {
                console.log("In aggregate")
                collection.aggregate(queryString,
                    function(err, searchRes) {
                        if (err) {
                            responseObject.statusCode = -1;
                            responseObject.message = "Server Error";
                            next(responseObject)
                        } else {
                            console.log("Search result", searchRes);
                            responseObject.statusCode = 0;
                            responseObject.message = "Search result fetched";
                            responseObject.result = searchRes;
                            next(responseObject);
                        } //endof function callback
                    });
            }
        });
    },


    searchMyForumQues: function(searchtext, skip, limit, next) {
        var responseObject = {};
        console.log(searchtext);

        var queryString = [{
            $match: {
                $or: [{ "frontendUserInfo.email": searchtext }, { "frontendUserInfo.MobileNumber": searchtext }]
            }
        }];

        queryString.push({
            $sort: {
                "updatedAt": -1
            }
        });

        if (skip !== undefined) {
            queryString.push({ $skip: skip });
        }

        if (limit !== undefined) {
            queryString.push({ $limit: limit });
        }

        queryString.push({
            $project: {
                "question": "$question",
                "questionId": "$questionId",
                "categoryId": "$categoryId",
                "subCategoryId": "$subCategoryId"
            }
        });

        queryString.push({
            $lookup: {
                "from": "ForumAnswers",
                "localField": "questionId",
                "foreignField": "questionId",
                "as": "user1"
            }
        });

        queryString.push({
            $project: {
                "question": "$question",
                "questionId": "$questionId",
                "answerId": { "$slice": ["$user1.answerId", -1] },
                "answer": { "$slice": ["$user1.answer", -1] }
            }
        });
        ForumQuestions.native(function(err, collection) {
            if (err) {
                responseObject.statusCode = -1;
                responseObject.message = "Server Error";
                next(responseObject)
            } else {
                console.log("In aggregate")
                collection.aggregate(queryString,
                    function(err, searchRes) {
                        if (err) {
                            responseObject.statusCode = -1;
                            responseObject.message = "Server Error";
                            next(responseObject)
                        } else {
                            console.log("Search result", searchRes);
                            responseObject.statusCode = 0;
                            responseObject.message = "Search result fetched";
                            responseObject.result = searchRes;
                            next(responseObject);
                        } //endof function callback
                    });
            }
        });
    },

    searchEachForumQuestion: function(question, skip, limit, next) {
        var responseObject = {};
        console.log(question);

        var queryString = [{
            $match: {
                "question": question
            }
        }];

        /* queryString.push({
            $sort: {
                "updatedAt": -1
            }
        });
*/
        if (skip !== undefined) {
            queryString.push({ $skip: skip });
        }

        if (limit !== undefined) {
            queryString.push({ $limit: 5 });
        }
        queryString.push({
            $lookup: {
                "from": "ForumAnswers",
                "localField": "questionId",
                "foreignField": "questionId",
                "as": "questionreply"
            }
        });

        queryString.push({
            $project: {
                "_id": 0,
                "questionId": "$questionId",
                "question": "$question",
                "answer": "$questionreply.answer",
                "answerId": "$questionreply.answerId",
            }
        });

        ForumQuestions.native(function(err, collection) {
            if (err) {
                responseObject.statusCode = -1;
                responseObject.message = "Server Error";
                next(responseObject)
            } else {
                console.log("In aggregate query", queryString)
                collection.aggregate(queryString,
                    function(err, searchRes) {
                        if (err) {
                            responseObject.statusCode = -1;
                            responseObject.message = "Server Error";
                            next(responseObject)
                        } else {
                            console.log("Search result", searchRes);
                            responseObject.statusCode = 0;
                            responseObject.message = "Search result fetched";
                            responseObject.result = searchRes;
                            next(responseObject);
                        } //endof function callback
                    });
            }
        });
    },
};