/**
 * ForumAnswersController
 *
 * @description :: Server-side logic for managing Forumanswers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    addAnswer:function(req,res){
        ForumAnswers.addAnswer(req.body,function(response){
            res.json(response);
        })
    },

    updateAnswer:function(req,res){
        ForumAnswers.updateAnswer(req.body,function(response){
            res.json(response);
        })
    },

    getAnswer:function(req,res){
        ForumAnswers.getAnswer(req.body,function(response){
            res.json(response);
        })
    }
};

