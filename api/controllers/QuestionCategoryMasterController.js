/**
 * QuestionCategoryMasterController
 *
 * @description :: Server-side logic for managing Questioncategorymasters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    addCategory:function(req,res){
        QuestionCategoryMaster.addCategory(req.body,function(response){
            res.json(response);
        })
    },
    getCategory:function(req,res){
        QuestionCategoryMaster.getCategory(req.body,function(response){
            res.json(response);
        })
    },
    updateCategory:function(req,res){
        QuestionCategoryMaster.updateCategory(req.body,function(response){
            res.json(response);
        })
    },
    addSubCategory:function(req,res){
        QuestionCategoryMaster.addSubCategory(req.body,function(response){
            res.json(response);
        })
    },
    updateSubCategory:function(req,res){
        QuestionCategoryMaster.updateSubCategory(req.body,function(response){
            res.json(response);
        })
    },
    getsubCategory: function(req, res) {
        QuestionCategoryMaster.getsubCategory(req.body, function(response) {
            res.json(response);
        })
    },

};

