/**
 * ForumQuestionsController
 *
 * @description :: Server-side logic for managing Forumquestions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var request = require('request');
module.exports = {
    /*
  	# Method: addSecondarySale
  	# Description: to add primary sales in database
  	*/
    addForumQuestion: function(req, res) {
        var token = req.token;
        var programInfo = req.programObject;
        console.log("token", token);
        //Variable defined to create userLogObject
        var userLogObject = req.body.frontendUserInfo;
        // Token hardcoded as of now
        token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ5MzcwNzk2MjMzNCIsImlhdCI6MTUyNzMxMDE1MH0.2iy7CrgRir9wwH-fSnQvVyoSxy_t55ClerM-GCo4pb2ZNQwpRy54UoMEHnHkE0y5WQxU8RlD1VkWioygY0fBMQ";

        // userLogObject.requestApi = req.baseUrl + req.path;
        // userLogObject.event = "Add Secondary Sale";
        // userLogObject.eventType = "Add";

        ForumQuestions.addForumQuestion(req.body, token, programInfo, function(response) {
            res.json(response);
            // userLogObject.response = response.message;
            // userLogObject.responseData = response;
            // LogService.addUserLog(userLogObject, token);
        });
    },

    updateForumQuestion: function(req, res) {
        var token = req.token;
        var programInfo = req.programObject;

        // //Variable defined to create userLogObject
        // var userLogObject = req.body.frontendUserInfo;
        // userLogObject.requestApi = req.baseUrl + req.path;
        // userLogObject.event = "Update Secondary sales";
        // userLogObject.eventType = "Update";

        ForumQuestions.updateForumQuestion(req.body, token, programInfo, function(response) {
            res.json(response);
            // var resObj = JSON.stringify(response);
            // resObj = JSON.parse(resObj);
            // userLogObject.responseData = resObj;
            // userLogObject.response = response.message;
            // LogService.addUserLog(userLogObject, token);
        });
    },

    getForumQuestions: function(req, res) {
        console.log("req.body", req.body);
        var token = req.token;


        // //Variable defined to create userLogObject
        // var userLogObject = req.body.frontendUserInfo;
        // userLogObject.requestApi = req.baseUrl + req.path;
        // userLogObject.event = "Fetch secondary sales";
        // userLogObject.eventType = "Get";

        ForumQuestions.getForumQuestions(req.body, function(response) {
            res.json(response);
            // userLogObject.response = response.message;
            // LogService.addUserLog(userLogObject, token);
        });
    },

    displaylastForumQuestions: function(req, res) {
        var skip = req.body.skip;
        var limit = req.body.limit;
        if (req.body.engineers == null) {
            ForumQuestions.displaylastForumQues(skip, limit, function(response) {
                res.json(response);
            });
        } else {
            var engineers = req.body.engineers;
            var skip = req.body.skip;
            var limit = req.body.limit
            console.log("Engg ", engineers);
            console.log(req.body.engineers);

            ForumAnswers.displaylastForumQuesforengineers(engineers, skip, limit, function(response) {
                res.json(response);
            });
        }
    },

    searchForumQuestions: function(req, res) {
        console.log(req.body.searchtext);
        ForumQuestions.searchForumQues(req.body.searchtext, req.body.skip, req.body.limit, function(response) {
            res.json(response);
        });

    },

    searchMyForumQuestions: function(req, res) {
        console.log(req.body.searchtext);
        ForumQuestions.searchMyForumQues(req.body.searchtext, req.body.skip, req.body.limit, function(response) {
            res.json(response);
        });

    },

    searchEachForumQues: function(req, res) {
        console.log(req.body.question);
        ForumQuestions.searchEachForumQuestion(req.body.question, req.body.skip, req.body.limit, function(response) {
            res.json(response);
        });
    },
};