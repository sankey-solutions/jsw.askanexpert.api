/**
 * Programusers
 *
 * @description :: Server-side logic for managing ProgramUsers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var request = require('request');
module.exports = {
    createProfileDetails: function(req, res) {
        console.log("AAAAAAAAAAAAAAAAAAAAAAA");
        console.log(req.body.programId);
        ProgramUsers.createExpertProfileDetails(req.body, function(response) {
            res.json(response);
        })
    },

    updateProfileDetails: function(req, res) {
        ProgramUsers.updateExpertProfileDetails(req.body.programId, function(response) {
            res.json(response);
        })
    },

    displayProfileDetails: function(req, res) {
        console.log(req.body.programId);
        console.log(req.body.engineerId);
        ProgramUsers.showexpertProfileDetails(req.body, function(response) {
            res.json(response);
        });
    },

    getExpertList: function(req, res) {

        ProgramUsers.getExpertList(req, function(response) {
            res.json(response);
        });
    },
    /*
    mapProfileId: function(req, res) {
        console.log(req.body.programUserId);
        ProgramUsers.mapProfileId(req.body.programUserId, function(response) {
            res.json(response);
        });
    },*/
};