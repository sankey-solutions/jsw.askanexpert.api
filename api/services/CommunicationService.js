/**
 * CommunicationService.js
 *
 * @description 	 :: Communication Service :  To send sms,email and push notifications to users
 * @created          :: Ajeet
 * @Created  Date    :: 02/04/2016
 * @lastEdited       :: 
 * @lastEdited Date  :: 
 *
 */

"use strict";

var request = require('request');

module.exports = {



    /*
    # Method: sendSms
    # Description: to SEND SMS TO USERS
    */

    sendSms: function(reqBody, token) {

        // default response object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null
        };

        var mobileNumber = reqBody.mobileNumber; // mobile no to which sms to be sent 
        var message = reqBody.message; // message to be sent
        // var communicationLogInfo = {} //request object to add communication logs

        // communicationLogInfo.communicationType = "SMS";
        // communicationLogInfo.sendTo = mobileNumber;
        // communicationLogInfo.message = message;
        // communicationLogInfo.userId = reqBody.userId;

        var communicationLogInfo = [];
        var userLength = reqBody.userIds.length;
        for (var i = 0; i < userLength; i++) {
            var logObject = {};

            logObject.communicationType = "SMS";
            logObject.sendTo = mobileNumber;
            logObject.message = message;
            logObject.userId = reqBody.userIds[i];

            communicationLogInfo.push(logObject);
        }
        var reqObject = { // json req object
            "mobileNumber": mobileNumber,
            "message": message,
            "communicationLogInfo": communicationLogInfo
        }
        console.log('communicationLogInfo', communicationLogInfo);
        //api to send sms
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().sendSms.port +
            ServConfigService.getApplicationAPIs().sendSms.url;


        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().sendSms.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };

        //console.log("requestOptions",requestOptions);

        // consuming the SMS API
        request(requestOptions, function(error, response, body) {
            //console.log("SMS service body is",body);
            if (error || !body) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming SMS API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "SMS sent Successfully!!";
                responseObj.result = mobileNumber;
            } else {
                responseObj = body;
            }
            //next(responseObj);
        });

    },

    //--------------------------------------SEND EMAIL--------------------------------------------------------------------//

    /*
    # Method: sendEmail
    # Description: to sendEmail to users
    */

    sendEmail: function(reqBody, token) {

        //send responseObject
        var responseObj = {
            "statusCode": -1,
            "message": null
        }
        var programId = reqBody.programId;
        var programName = reqBody.programName
        var programUserId = reqBody.programUserId;
        var clientId = reqBody.clientId;
        var programUsersIdArray = [];
        // programUsersIdArray = reqBody.programUsersIdArray;
        programUsersIdArray.push(reqBody.programUserId);


        // var communicationLogInfoObj = {} //request object to add communication logs

        // communicationLogInfoObj.communicationType = "Email";
        // communicationLogInfoObj.from = "info@annectos.in";
        // communicationLogInfoObj.to = reqBody.emailId;
        // communicationLogInfoObj.subject = reqBody.subject;
        // communicationLogInfoObj.text = reqBody.text;
        // var communicationLogInfo = [];
        // communicationLogInfo.push(communicationLogInfoObj);
        var communicationLogInfo = [];

        var userLength = reqBody.userIds.length;

        for (var i = 0; i < userLength; i++) {
            var logObject = {};

            logObject.communicationType = "EMAIL";
            logObject.sendTo = reqBody.emailIds[i];
            logObject.message = reqBody.html;
            logObject.userId = reqBody.userIds[i];

            communicationLogInfo.push(logObject);
        }


        var html = [];
        // html.message = "";
        var Unsubscribe = "Unsubscribe";
        for (var a = 0; a < userLength; a++) {
            console.log("here");
            // html.message = "";
            html[a] = "";
            html[a] = reqBody.html + "<br>" + "<br>" + "<br>" + "<br>" + "You are receiving this email as part of your participation in Loyalty Program" + " " + programName + "." + " " + "If you want to unsubscribe from this mailer, please click <a href=http://localhost:1335/unsubscribe" + "?" + programId + "," + programUserId + "," + clientId + ">" + "" + Unsubscribe
        }
        //reqObject for sending email
        var reqObject = {
            from: "info@annectos.in",
            to: reqBody.emailIds,
            subject: reqBody.subject,
            text: html,
            html: html,
            communicationLogInfo: communicationLogInfo
        }

        console.log('Program reqBody.isProgramEmail', reqBody.isProgramEmail);
        if (reqBody.isProgramEmail) {
            //reqObject for sending email
            var reqObject = {
                from: reqBody.programEmailId,
                to: reqBody.emailIds,
                subject: reqBody.subject,
                text: html,
                html: html,
                isProgramEmail: true,
                "communicationLogInfo": communicationLogInfo,
                subscribeObject: {
                    programId: reqBody.programId,
                    programUsersIdArray: programUsersIdArray,
                    clientId: reqBody.clientId,
                },
                userIds: reqBody.userIds
            }


        } else {
            //reqObject for sending email
            var reqObject = {
                from: "info@annectos.in",
                to: reqBody.emailIds,
                subject: reqBody.subject,
                text: html,
                html: html,
                "communicationLogInfo": communicationLogInfo,
                subscribeObject: {
                    programId: reqBody.programId,
                    programUserId: reqBody.programUserId,
                    clientId: reqBody.clientId,
                },
                userIds: reqBody.userIds
            }
        }


        //api to send sms
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().sendEmailSubscriptionBased.port +
            ServConfigService.getApplicationAPIs().sendEmailSubscriptionBased.url;


        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().sendEmailSubscriptionBased.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };
        console.log("requestOptions", requestOptions);
        //consuming email api
        request(requestOptions, function(error, response, body) {
            console.log("body is", body, "error is", error);
            if (error || !body) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming Email API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Email sent successfully !!";
                responseObj.result = body.result;
            } else {
                responseObj = body;
            }

        });
    },

    /*
    # Method: sendEmail
    # Description: to sendEmail to users
    */

    sendPushNotification: function(reqBody, token) {

        console.log("notification service called");
        //send responseObject
        var responseObj = {
            "statusCode": -1,
            "message": null
        }

        var notificationDetails = {
            "title": reqBody.title,
            "message": reqBody.message,
            "targetScreen": reqBody.targetScreen
        }

        var communicationLogInfoObj = {} //request object to add communication logs

        communicationLogInfoObj.communicationType = "PushNotification";
        communicationLogInfoObj.sendTo = reqBody.userId;
        communicationLogInfoObj.message = reqBody.message;
        communicationLogInfoObj.userId = reqBody.userId;
        communicationLogInfoObj.programId = reqBody.programId;
        var communicationLogInfo = [];
        communicationLogInfo.push(communicationLogInfoObj);

        //reqObject for sending pushNotification
        var reqObject = {
            userIds: reqBody.userId,
            notificationDetails: notificationDetails,
            "communicationLogInfo": communicationLogInfo
        }

        //api to send sms
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().sendPushNotification.port +
            ServConfigService.getApplicationAPIs().sendPushNotification.url;


        //request options to fetch program user
        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().sendPushNotification.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };

        //console.log("requestOptions",requestOptions);
        //consuming email api
        request(requestOptions, function(error, response, body) {
            //console.log("body is",body,"error is",error);
            if (error || !body) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming notification API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Notification sent successfully !!";
                //responseObj.result = body.result;
            } else {
                responseObj = body;
            }

        });
    },

    //Fetch all transactional event filtered by module name
    getTransactionalEvents: function(reqBody, userLogObject, token, next) {

        var responseObj = {
            "statusCode": -1,
            "message": null,
            "smsInfo": {},
            "emailInfo": {},
            "notificationInfo": {}
        }
        var records;

        var reqObject = {
            moduleName: "Program",
            frontendUserInfo: userLogObject,
            programId: reqBody.programId,
            clientId: reqBody.clientId
        }
        var apiURL = ServConfigService.getApplicationConfig().base_url +
            ":" +
            ServConfigService.getApplicationAPIs().getTransactionalEvent.port +
            ServConfigService.getApplicationAPIs().getTransactionalEvent.url;

        var requestOptions = {
            url: apiURL,
            method: ServConfigService.getApplicationAPIs().getTransactionalEvent.method,
            headers: {
                'authorization': 'Bearer ' + token
            },
            json: reqObject
        };

        //console.log("requestOptions", requestOptions);
        request(requestOptions, function(error, response, body) {
            //console.log("body is", body, "error is", error);
            if (error || body === undefined) { // some error occured
                responseObj.statusCode = -1;
                responseObj.message = "Error occured while consuming notification API";
            } else if (body.statusCode === 0) {
                responseObj.statusCode = 0;
                responseObj.message = "Events fetched successfully !!";
                responseObj.result = body.result
                records = body.result;

                for (var i = 0; i < records.length; i++) {
                    for (var j = 0; j < records[i].recordInfo.length; j++) {

                        if (records[i].recordInfo[j].subModuleName === "ClaimPending") {
                            if (records[i].communicationType === "SMS") {
                                responseObj.smsInfo.claimPending = records[i].recordInfo[j];
                            } else if (records[i].communicationType === "Email") {
                                responseObj.emailInfo.claimPending = records[i].recordInfo[j];
                            } {
                                responseObj.notificationInfo.claimPending = records[i].recordInfo[j];
                            }
                            //break;
                        } else if (records[i].recordInfo[j].subModuleName === "ClaimApproval") {
                            if (records[i].communicationType === "SMS") {
                                responseObj.smsInfo.claimApproval = records[i].recordInfo[j];
                            } else if (records[i].communicationType === "Email") {
                                responseObj.emailInfo.claimApproval = records[i].recordInfo[j];
                            } else {
                                responseObj.notificationInfo.claimApproval = records[i].recordInfo[j];
                            }
                            //break;
                        } else if (records[i].recordInfo[j].subModuleName === "ClaimRejection") {
                            if (records[i].communicationType === "SMS") {
                                responseObj.smsInfo.claimRejection = records[i].recordInfo[j];
                            } else if (records[i].communicationType === "Email") {
                                responseObj.emailInfo.claimRejection = records[i].recordInfo[j];
                            } else {
                                responseObj.notificationInfo.claimRejection = records[i].recordInfo[j];
                            }
                            //break;
                        }
                    }
                }
            } else {
                responseObj = body;
            }
            //console.log("responseObj", responseObj);
            next(responseObj);
        });
    }


}