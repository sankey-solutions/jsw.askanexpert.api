/**
 * Common Operations.js
 *
 * @description     :: To perform common operations  especially ROLLBACK
 * @createdBy       :: Swati
 * @created Date    :: 01/02/2016
 * @Last edited by  :: 
 * @last edited date::
 */

"use strict";

module.exports = {

    /*
	# Method: findRecord
	# Description: to find any record in database
	# Input: Model name,array of IDs to find and callback function
	# Output : response with success or error
  	*/

    findRecord: function(Model, ids, token, next) {

        // default response object
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };
        var Model;
        var queryString = { programId: ids };
        if (Model === "ProgramSetup") {
            Model = ProgramSetup;
        }

        Model.find(queryString).exec(function(error, foundRecords) {
            if (error || !foundRecords) {
                console.log("Unable to find record", err, foundRecords);
                responseObj.statusCode = -2;
                responseObj.message = "Fatal Integrity Error : Failed to found records";
                responseObj.result = ids;
                //sails.log.error("CommonOperations>findRecord>BusinessEntityUserMaster.destroy ","records to be find:"+ids," error:"+error);
                return next(responseObj);
            } else {
                if (foundRecords.length === 0) {
                    responseObj.statusCode = -2;
                    responseObj.message = "Fatal Integrity Error ";
                    responseObj.result = ids;
                    //sails.log.error("CommonOperations>findRecord ","records to be founds:"+ids," error:"+error);
                    return next(responseObj);
                } else {
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully found";
                    responseObj.result = foundRecords;
                    //sails.log.info("CommonOperations>findRecord>BusinessEntityUserMaster.destroy: record is successfully destroyed");
                    return next(responseObj);
                }
            }
        });
    },
    //----------------------------------------------------------------------------------------------------------//

    /*
    # Method: findCount
    # Description: to find count of all records in a collection
    # Input: Model name,token,callback function
    # Output : response with success or error
    */

    findCount: function(Model, countQueryString, next) {
        // default response object
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };


        var collectionName = Model;
        console.log("Finding Count of:", Model);


        switch (Model) {
            case 'ForumQuestions':
                Model = ForumQuestions;
                break;
            case 'SecondarySales':
                Model = SecondarySales;
                break;
            case 'DisplayContest':
                Model = DisplayContest;
                break;
            case 'BonusPoints':
                Model = BonusPoints;
                break;
            case 'UserNotifications':
                Model = UserNotifications;
                break;
        }

        console.log("Finding Count of:", Model);

        Model.count(countQueryString).exec(function(err, foundCount) {
            if (err || !foundCount) {
                responseObj.message = "Count cound not be fetched: " + err;
                console.log("Finding count Failure:", err);
                sails.log.error("CommonOperations > findCount > " + collectionName + "Error" + err);
            } else {
                console.log("counts found:", foundCount);
                responseObj.statusCode = 0;
                responseObj.result = foundCount;
                responseObj.message = "counts fetched successfully";
                sails.log.info("CommonOperations > findCount > " + collectionName + "Counts fetched successfully");
            }
            next(responseObj);
        });

    },

    /*
    # Method: deleteRecord
    # Description: to delete any record in database
    # Input: Model name,array of IDs to delete and callback function
    # Output : response with success or error
    */
    deleteRecord: function(Model, fieldName, ids, token, next) {

        // default response object
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };

        var queryString;
        //switch case defined to match model name
        switch (Model) {
            case 'SecondarySales':
                Model = SecondarySales;
                break;
        }

        //switch case defined to match field name
        switch (fieldName) {
            case 'secondarySalesId':
                queryString = { secondarySalesId: ids };
                break;

        }

        Model.destroy(queryString).exec(function(error, deletedRecords) {
            if (error || !deletedRecords) {
                responseObj.statusCode = -2;
                responseObj.message = "Fatal Integrity Error : Failed to delete record in case of rollback";
                responseObj.result = businessId;
                sails.log.error("CommonOperations>deleteRecord>" + Model + ".destroy >records to be deleted:" + ids, " error:" + error);

            } else {
                if (deletedRecords.length === 0) {
                    responseObj.statusCode = -2;
                    responseObj.message = "Fatal Integrity Error : Failed to delete record in case of rollback";
                    sails.log.error("CommonOperations>deleteRecord>" + Model + ".destroy > records to be deleted:" + ids, " result:" + deletedRecords);

                } else { // success: records deleted
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully deleted";
                    sails.log.info("CommonOperations>deleteRecord>" + Model + ".destroy > record is successfully destroyed");

                }
            }
            next(responseObj);
        });
    },

    /*
    # Method: updateRecord
    # Description: to update record in database
    # Input: Model name,array of IDs to update,infirmation to update and callback function
    # Output : response with success or error
    */
    updateRecord: function(Model, fieldName, ids, updateInfo, token, next) {

        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };

        var queryString;

        //switch case defined to match model name
        var queryString;
        //switch case defined to match model name
        switch (Model) {
            case 'SecondarySales':
                Model = SecondarySales;
                break;
        }

        //switch case defined to match field name
        switch (fieldName) {
            case 'secondarySalesId':
                queryString = { secondarySalesId: ids, isDeleted: false };
                break;
        }

        /*console.log("queryString",queryString);
        console.log("updateInfo",updateInfo);*/

        Model.update(queryString, updateInfo).exec(function(error, updatedRecords) {
            //console.log("updatedRecords",updatedRecords);
            if (error || !updatedRecords) {
                responseObj.statusCode = -1;
                responseObj.message = "Record Updation Failed" + error;
                sails.log.error("CommonOperations>updateRecord>" + Model + ".update ", "records to be updated:" + ids, " error:" + error);
                next(responseObj);
            } else {
                if (updatedRecords.length === 0) { // no records found or some error
                    responseObj.statusCode = 2;
                    responseObj.message = "No Record found!!!";
                    sails.log.error("CommonOperations>updateRecord>" + Model + ".update ", "records to be updated:" + ids, " error:" + error);
                    next(responseObj);
                } else { // success: records updated
                    //console.log("updated records is",updatedRecords);
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully updated";
                    sails.log.info("CommonOperations>updateRecord>" + Model + ".update: record is successfully updated");
                    next(responseObj);
                }
            }
        });
    },


};