/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.connections.html
 */

module.exports.connections = {

  /***************************************************************************
   *                                                                          *
   * Local disk storage for DEVELOPMENT ONLY                                  *
   *                                                                          *
   * Installed by default.                                                    *
   *                                                                          *
   ***************************************************************************/
  localDiskDb: {
    adapter: 'sails-disk'
  },

  /***************************************************************************
   *                                                                          *
   * MySQL is the world's most popular relational database.                   *
   * http://en.wikipedia.org/wiki/MySQL                                       *
   *                                                                          *
   * Run: npm install sails-mysql@for-sails-0.12 --save                       *
   *                                                                          *
   ***************************************************************************/
  // someMysqlServer: {
  //   adapter: 'sails-mysql',
  //   host: 'YOUR_MYSQL_SERVER_HOSTNAME_OR_IP_ADDRESS',
  //   user: 'YOUR_MYSQL_USER', //optional
  //   password: 'YOUR_MYSQL_PASSWORD', //optional
  //   database: 'YOUR_MYSQL_DB' //optional
  // },
  phoenix2MongodbServer: {
    adapter: "sails-mongo",
    // host: 'ds151941.mlab.com',
    // port: 51941,
    // user: 'racoldDevelopment', //optional
    // password: 'racoldDevelopment', //optional
    // database: 'racoldevelopmentdb',
    socketOptions: {
      noDelay: true,
      connectTimeoutMS: 30000,
      socketTimeoutMS: 0
    },
    host: "ds213229.mlab.com",
    port: 13229,
    user: "admin", //optional
    password: "admin", //optional
    database: "jswdevelopmentdb"
    //database: 'phoenix2' //optional
  },

  phoenix2EC2testServer: {
    adapter: 'sails-mongo',
    host: 'qamongo.annectos.net',
    port: 27047,
    user: 'readwrite', //optional
    password: 'jsw',
    database: 'jswdevelopmentdatabase', //optional

  },

  phoenix2testServer: {
    adapter: 'sails-mongo',
    host: 'ds139959.mlab.com',
    port: 39959,
    user: 'phoenix2testdb',
    password: 'phoenix2testdb',
    database: 'phoenix2testdb'
  },

  prodNewServer: {
    adapter: 'sails-mongo',
    host: 'phoenix2mongo.goldencircle.in',
    port: 42611,
    // user: 'relaxoRW',
    // password: 'jsdyuio78o',
    // database: 'relaxo2017',
    socketOptions: {
      noDelay: true,
      connectTimeoutMS: 30000,
      socketTimeoutMS: 0
    },
    user: 'jswRW',
    password: 'gt091w',
    database: 'jsw'
  }
  /***************************************************************************
   *                                                                          *
   * MongoDB is the leading NoSQL database.                                   *
   * http://en.wikipedia.org/wiki/MongoDB                                     *
   *                                                                          *
   * Run: npm install sails-mongo@for-sails-0.12 --save                       *
   *                                                                          *
   ***************************************************************************/
  // someMongodbServer: {
  //   adapter: 'sails-mongo',
  //   host: 'localhost',
  //   port: 27017,
  //   user: 'username', //optional
  //   password: 'password', //optional
  //   database: 'your_mongo_db_name_here' //optional
  // },

  /***************************************************************************
   *                                                                          *
   * PostgreSQL is another officially supported relational database.          *
   * http://en.wikipedia.org/wiki/PostgreSQL                                  *
   *                                                                          *
   * Run: npm install sails-postgresql@for-sails-0.12 --save                  *
   *                                                                          *
   *                                                                          *
   ***************************************************************************/
  // somePostgresqlServer: {
  //   adapter: 'sails-postgresql',
  //   host: 'YOUR_POSTGRES_SERVER_HOSTNAME_OR_IP_ADDRESS',
  //   user: 'YOUR_POSTGRES_USER', // optional
  //   password: 'YOUR_POSTGRES_PASSWORD', // optional
  //   database: 'YOUR_POSTGRES_DB' //optional
  // }


  /***************************************************************************
   *                                                                          *
   * More adapters: https://github.com/balderdashy/sails                      *
   *                                                                          *
   ***************************************************************************/

};
